﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР2
{
    public partial class Form1 : Form
    {
        private DataTable DAIDataTable;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void LoadFromFile(string filePath)
        {
            DAIDataTable = new DataTable();
            DAIDataTable.Columns.Add("Прізвище", typeof(string));
            DAIDataTable.Columns.Add("Марка", typeof(string));
            DAIDataTable.Columns.Add("Номер", typeof(string));
            DAIDataTable.Columns.Add("Колір", typeof(string));

            try
            {
                //Зчитуємо з файлу 
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] scheduleEntry = line.Split(',');
                        if (scheduleEntry.Length == 4)
                        {
                            DAIDataTable.Rows.Add(scheduleEntry);
                        }
                    }
                }

                dataGridView1.DataSource = DAIDataTable;
            }
            catch (IOException ex)
            {
                MessageBox.Show("Помилка при читанні файлу: " + ex.Message);
            }
        }

        private void завантажитиЗФайлуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt";
            openFileDialog.Title = "Виберіть файл";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = openFileDialog.FileName;
                LoadFromFile(selectedFilePath);
            }
        }
        private void SearchBySurname(string searchValue)
        {
            int columnIndex2 = 0;


            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[columnIndex2].Value != null &&
                    row.Cells[columnIndex2].Value.ToString().Equals(searchValue, StringComparison.OrdinalIgnoreCase))
                {
                    row.DefaultCellStyle.BackColor = System.Drawing.Color.LightPink;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = dataGridView1.DefaultCellStyle.BackColor;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string searchValue = textBox1.Text.Trim();
            SearchBySurname(searchValue);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int count = CountCarNumbersStartingWithA(DAIDataTable);
            MessageBox.Show("Кількість номерів машин, які починаються на 'A': " + count);
        
        }
        private int CountCarNumbersStartingWithA(DataTable data)
        {
            int count = 0;

            foreach (DataRow row in data.Rows)
            {
                string number = row["Номер"].ToString();
                if (!string.IsNullOrEmpty(number) && number.StartsWith("А", StringComparison.OrdinalIgnoreCase))
                {
                    count++;
                }
            }

            return count;
        }
    }
}
